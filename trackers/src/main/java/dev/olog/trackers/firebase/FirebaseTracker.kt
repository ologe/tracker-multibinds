package dev.olog.trackers.firebase

import dev.olog.trackers.IAmATrackerImplementation
import dev.olog.trackers.ScreenTracker
import javax.inject.Inject

internal class FirebaseTracker @Inject constructor(

) : IAmATrackerImplementation, ScreenTracker {


    override fun trackScreen(screenName: String) {
        println("tracking screen=$screenName from firebase")
    }
}
