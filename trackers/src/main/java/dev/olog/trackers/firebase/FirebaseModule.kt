package dev.olog.trackers.firebase

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet
import dev.olog.trackers.IAmATrackerImplementation
import javax.inject.Singleton

@Module
internal abstract class FirebaseModule {

    @Binds
    @Singleton
    @IntoSet
    internal abstract fun provideTracker(impl: FirebaseTracker): IAmATrackerImplementation

}