package dev.olog.trackers.ga

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet
import dev.olog.trackers.IAmATrackerImplementation
import javax.inject.Singleton

@Module
internal abstract class GAModule {

    @Binds
    @Singleton
    @IntoSet
    internal abstract fun provideTracker(impl: GATracker): IAmATrackerImplementation

}