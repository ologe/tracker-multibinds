package dev.olog.trackers.ga

import dev.olog.trackers.IAmATrackerImplementation
import dev.olog.trackers.LifecycleTracker
import dev.olog.trackers.ScreenTracker
import javax.inject.Inject

internal class GATracker @Inject constructor(

) : IAmATrackerImplementation, LifecycleTracker by GALifecycleTracker(), ScreenTracker {

    override fun trackScreen(screenName: String) {
        println("track screen $screenName from ga")
    }
}

