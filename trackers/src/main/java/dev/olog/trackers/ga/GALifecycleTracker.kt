package dev.olog.trackers.ga

import dev.olog.trackers.LifecycleTracker

internal class GALifecycleTracker : LifecycleTracker {
    override fun trackOnStart() {
        println("on start from ga")
    }

    override fun trackOnStop() {
        println("on stop from ga")
    }
}