package dev.olog.trackers

import dagger.Binds
import dagger.Module
import dagger.multibindings.Multibinds
import dev.olog.trackers.firebase.FirebaseModule
import dev.olog.trackers.ga.GAModule
import javax.inject.Singleton

/**
 * Other trackers are pluggable to this multibinds
 */
@Module(
    includes = [
        GAModule::class,
        FirebaseModule::class
    ]
)
abstract class TrackersModule {

    @Binds
    @Singleton
    internal abstract fun provideFacade(impl: TrackerFacade): ITracker

    @Multibinds
    internal abstract fun provideTrackers(): Set<IAmATrackerImplementation>

}