package dev.olog.trackers

/**
 * Can be just one big facade that exposes all methods, or multiple
 * trackers based on features, etc..
 */
interface ITracker : LifecycleTracker, ScreenTracker

interface LifecycleTracker {

    fun trackOnStart()
    fun trackOnStop()

}

interface ScreenTracker {

    fun trackScreen(screenName: String)

}

internal interface IAmATrackerImplementation