package dev.olog.trackers.executor

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.asCoroutineDispatcher
import java.util.concurrent.Executors

private val dispatchers = Executors
    .newSingleThreadExecutor() // TODO or a pool
    .asCoroutineDispatcher()

@Suppress("FunctionName")
internal fun TrackerScope(): CoroutineScope = CoroutineScope(SupervisorJob() + dispatchers)