package dev.olog.trackers

import dev.olog.trackers.executor.TrackerScope
import dev.olog.trackers.utils.launchUnit
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

internal class TrackerFacade @Inject constructor(
    private val trackers: Set<@JvmSuppressWildcards IAmATrackerImplementation>
) : ITracker, CoroutineScope by TrackerScope() {

    override fun trackOnStart() = launchUnit {
        trackers.filterIsInstance<LifecycleTracker>()
            .forEach { it.trackOnStart() }
    }

    override fun trackOnStop() = launchUnit {
        trackers.filterIsInstance<LifecycleTracker>()
            .forEach { it.trackOnStop() }
    }

    override fun trackScreen(screenName: String) = launchUnit {
        trackers.filterIsInstance<ScreenTracker>()
            .forEach {
                it.trackScreen(screenName)
            }
    }

}