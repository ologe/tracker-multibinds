package dev.olog.trackers.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

internal fun CoroutineScope.launchUnit(action: () -> Unit) {
    launch { action() }
}