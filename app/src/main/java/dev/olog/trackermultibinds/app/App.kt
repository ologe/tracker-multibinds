package dev.olog.trackermultibinds.app

import dagger.Component
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.android.support.DaggerApplication
import dev.olog.trackermultibinds.presentation.DetailActivity
import dev.olog.trackermultibinds.presentation.MainActivity
import dev.olog.trackers.TrackersModule
import javax.inject.Singleton

class App : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(this)
    }
}

@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,

        TrackersModule::class
    ]
)
@Singleton
interface AppComponent : AndroidInjector<App> {

    @Component.Factory
    interface Factory : AndroidInjector.Factory<App>

}

@Module
abstract class AppModule {

    @ContributesAndroidInjector
    abstract fun provideMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun provideDetailActivity(): DetailActivity

}

