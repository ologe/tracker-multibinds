package dev.olog.trackermultibinds.presentation

import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import dev.olog.trackermultibinds.R
import dev.olog.trackers.ITracker
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class DetailActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var tracker: ITracker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView.text = DetailActivity::class.java.simpleName

        tracker.trackScreen(DetailActivity::class.java.simpleName)
    }

}