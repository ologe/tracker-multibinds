package dev.olog.trackermultibinds.presentation

import android.content.Intent
import android.os.Bundle
import android.view.View
import dagger.android.support.DaggerAppCompatActivity
import dev.olog.trackermultibinds.R
import dev.olog.trackers.ITracker
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var trackerFacade: ITracker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView.text = MainActivity::class.java.simpleName

        trackerFacade.trackScreen(MainActivity::class.java.simpleName)

        findViewById<View>(android.R.id.content).setOnClickListener {
            startActivity(Intent(this, DetailActivity::class.java))
        }
    }

    override fun onStart() {
        super.onStart()
        trackerFacade.trackOnStart()
    }

    override fun onStop() {
        super.onStop()
        trackerFacade.trackOnStop()
    }

}
